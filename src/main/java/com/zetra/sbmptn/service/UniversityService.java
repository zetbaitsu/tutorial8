package com.zetra.sbmptn.service;

import com.zetra.sbmptn.model.University;

import java.util.List;

/**
 * Created on : April 07, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
public interface UniversityService {
    University selectUniversity(String code);

    List<University> selectAllUniversities();

    void insertUniversity(University university);

    void deleteUniversity(String code);

    void updateUniversity(University university);
}
