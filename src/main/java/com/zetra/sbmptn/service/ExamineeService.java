package com.zetra.sbmptn.service;

import com.zetra.sbmptn.model.Examinee;

import java.util.List;

/**
 * Created on : April 07, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
public interface ExamineeService {
    Examinee selectExaminee(String number);

    List<Examinee> selectProgramExaminees(String programCode);

    List<Examinee> selectUniversityExaminees(String univCode);

    void insertExaminee(Examinee examinee);

    void deleteExaminee(String number);

    void updateExaminee(Examinee examinee);
}
