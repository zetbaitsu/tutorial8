package com.zetra.sbmptn.service;

import com.zetra.sbmptn.model.StudyProgram;

import java.util.List;

/**
 * Created on : April 07, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
public interface StudyProgramService {
    StudyProgram selectStudyProgram(String code);

    List<StudyProgram> selectStudyPrograms(String univCode);

    void insertStudyProgram(StudyProgram studyProgram);

    void deleteStudyProgram(String code);

    void updateStudyProgram(StudyProgram studyProgram);
}
