package com.zetra.sbmptn.service;

import com.zetra.sbmptn.dao.StudyProgramMapper;
import com.zetra.sbmptn.model.StudyProgram;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created on : April 07, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
@Service
public class StudyProgramServiceDatabase implements StudyProgramService {
    private final StudyProgramMapper studyProgramMapper;

    @Autowired
    public StudyProgramServiceDatabase(StudyProgramMapper studyProgramMapper) {
        this.studyProgramMapper = studyProgramMapper;
    }

    @Override
    public StudyProgram selectStudyProgram(String code) {
        return studyProgramMapper.selectStudyProgram(code);
    }

    @Override
    public List<StudyProgram> selectStudyPrograms(String univCode) {
        return studyProgramMapper.selectStudyPrograms(univCode);
    }

    @Override
    public void insertStudyProgram(StudyProgram studyProgram) {
        studyProgramMapper.insertStudyProgram(studyProgram);
    }

    @Override
    public void deleteStudyProgram(String code) {
        studyProgramMapper.deleteStudyProgram(code);
    }

    @Override
    public void updateStudyProgram(StudyProgram studyProgram) {
        studyProgramMapper.updateStudyProgram(studyProgram);
    }
}
