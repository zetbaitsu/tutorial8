package com.zetra.sbmptn.service;

import com.zetra.sbmptn.model.ErrorMessage;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * Created on : April 07, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
@Service
public class ErrorServiceImpl implements ErrorService {
    public ErrorMessage generateErrorMessage(HttpServletRequest request) {
        int errorCode = (int) request.getAttribute("javax.servlet.error.status_code");
        switch (errorCode) {
            case 401:
                return new ErrorMessage("Halaman tidak dapat diakses", "Mohon maaf halaman ini tidak dapat diakses oleh anda.");
            case 404:
                return new ErrorMessage("Halaman tidak ditemukan", "Mohon maaf halaman yang anda cari tidak dapat ditemukan");
            case 500:
                return new ErrorMessage("Internal Server Error", "Terjadi kesalahan pada server, silahkan coba beberapa saat lagi.");
            default:
                return new ErrorMessage("Error", "Terjadi suatu kesalahan");
        }
    }
}