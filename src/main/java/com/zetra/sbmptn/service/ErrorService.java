package com.zetra.sbmptn.service;

import com.zetra.sbmptn.model.ErrorMessage;

import javax.servlet.http.HttpServletRequest;

/**
 * Created on : April 08, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
public interface ErrorService {
    ErrorMessage generateErrorMessage(HttpServletRequest request);
}
