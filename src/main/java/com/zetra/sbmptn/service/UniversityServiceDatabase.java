package com.zetra.sbmptn.service;

import com.zetra.sbmptn.dao.UniversityMapper;
import com.zetra.sbmptn.model.University;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created on : April 07, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
@Service
public class UniversityServiceDatabase implements UniversityService {
    private final UniversityMapper universityMapper;

    @Autowired
    public UniversityServiceDatabase(UniversityMapper universityMapper) {
        this.universityMapper = universityMapper;
    }

    @Override
    public University selectUniversity(String code) {
        return universityMapper.selectUniversity(code);
    }

    @Override
    public List<University> selectAllUniversities() {
        return universityMapper.selectAllUniversities();
    }

    @Override
    public void insertUniversity(University university) {
        universityMapper.insertUniversity(university);
    }

    @Override
    public void deleteUniversity(String code) {
        universityMapper.deleteUniversity(code);
    }

    @Override
    public void updateUniversity(University university) {
        universityMapper.updateUniversity(university);
    }
}
