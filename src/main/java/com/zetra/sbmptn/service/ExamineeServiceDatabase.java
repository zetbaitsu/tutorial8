package com.zetra.sbmptn.service;

import com.zetra.sbmptn.dao.ExamineeMapper;
import com.zetra.sbmptn.model.Examinee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created on : April 07, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
@Service
public class ExamineeServiceDatabase implements ExamineeService {
    private final ExamineeMapper examineeMapper;

    @Autowired
    public ExamineeServiceDatabase(ExamineeMapper examineeMapper) {
        this.examineeMapper = examineeMapper;
    }

    @Override
    public Examinee selectExaminee(String number) {
        return examineeMapper.selectExaminee(number);
    }

    @Override
    public List<Examinee> selectProgramExaminees(String programCode) {
        return examineeMapper.selectProgramExaminees(programCode);
    }

    @Override
    public List<Examinee> selectUniversityExaminees(String univCode) {
        return examineeMapper.selectUniversityExaminees(univCode);
    }

    @Override
    public void insertExaminee(Examinee examinee) {
        examineeMapper.insertExaminee(examinee);
    }

    @Override
    public void deleteExaminee(String number) {
        examineeMapper.deleteExaminee(number);
    }

    @Override
    public void updateExaminee(Examinee examinee) {
        examineeMapper.updateExaminee(examinee);
    }
}
