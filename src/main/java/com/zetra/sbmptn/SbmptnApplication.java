package com.zetra.sbmptn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created on : April 07, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
@SpringBootApplication
public class SbmptnApplication {
    public static void main(String[] args) {
        SpringApplication.run(SbmptnApplication.class, args);
    }
}
