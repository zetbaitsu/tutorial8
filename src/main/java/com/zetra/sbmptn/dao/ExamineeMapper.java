package com.zetra.sbmptn.dao;

import com.zetra.sbmptn.model.Examinee;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created on : April 07, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
@Mapper
public interface ExamineeMapper {
    @Select("select * from peserta where nomor = #{number}")
    @Results(value = {
            @Result(property = "number", column = "nomor"),
            @Result(property = "name", column = "nama"),
            @Result(property = "birthDate", column = "tgl_lahir"),
            @Result(property = "programCode", column = "kode_prodi")})
    Examinee selectExaminee(@Param("number") String number);

    @Select("select * from peserta where kode_prodi = #{programCode}")
    @Results(value = {
            @Result(property = "number", column = "nomor"),
            @Result(property = "name", column = "nama"),
            @Result(property = "birthDate", column = "tgl_lahir"),
            @Result(property = "programCode", column = "kode_prodi")})
    List<Examinee> selectProgramExaminees(@Param("programCode") String programCode);

    @Select("select * from peserta, prodi where peserta.kode_prodi = prodi.kode_prodi" +
            " and prodi.kode_univ = #{univCode}")
    @Results(value = {
            @Result(property = "number", column = "nomor"),
            @Result(property = "name", column = "nama"),
            @Result(property = "birthDate", column = "tgl_lahir"),
            @Result(property = "programCode", column = "kode_prodi")})
    List<Examinee> selectUniversityExaminees(@Param("univCode") String univCode);

    @Insert("INSERT INTO peserta(nomor, nama, tgl_lahir, kode_prodi) " +
            "VALUES (#{number}, #{name}, #{birthDate}, #{programCode})")
    void insertExaminee(Examinee examinee);

    @Delete("DELETE FROM peserta WHERE nomor = #{number}")
    void deleteExaminee(@Param("number") String number);

    @Update("UPDATE peserta SET nama = #{name}, tgl_lahir = #{birthDate}, " +
            "kode_prodi = #{programCode} WHERE nomor = #{number}")
    void updateExaminee(Examinee examinee);
}
