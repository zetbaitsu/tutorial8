package com.zetra.sbmptn.dao;

import com.zetra.sbmptn.model.University;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created on : April 07, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
@Mapper
public interface UniversityMapper {
    @Select("select * from univ where kode_univ = #{code}")
    @Results(value = {
            @Result(property = "code", column = "kode_univ"),
            @Result(property = "name", column = "nama_univ"),
            @Result(property = "url", column = "url_univ")})
    University selectUniversity(@Param("code") String code);

    @Select("select * from univ")
    @Results(value = {
            @Result(property = "code", column = "kode_univ"),
            @Result(property = "name", column = "nama_univ"),
            @Result(property = "url", column = "url_univ")})
    List<University> selectAllUniversities();

    @Insert("INSERT INTO univ(kode_univ, nama_univ, url_univ) VALUES (#{code}, #{name}, #{url})")
    void insertUniversity(University university);

    @Delete("DELETE FROM univ WHERE kode_univ = #{code}")
    void deleteUniversity(@Param("code") String code);

    @Update("UPDATE univ SET nama_univ = #{name}, url_univ = #{url} WHERE kode_univ = #{code}")
    void updateUniversity(University university);
}
