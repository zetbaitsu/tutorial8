package com.zetra.sbmptn.dao;

import com.zetra.sbmptn.model.StudyProgram;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created on : April 07, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
@Mapper
public interface StudyProgramMapper {
    @Select("select * from prodi where kode_prodi = #{code}")
    @Results(value = {
            @Result(property = "univCode", column = "kode_univ"),
            @Result(property = "code", column = "kode_prodi"),
            @Result(property = "name", column = "nama_prodi")})
    StudyProgram selectStudyProgram(@Param("code") String code);

    @Select("select * from prodi where kode_univ = #{univCode}")
    @Results(value = {
            @Result(property = "univCode", column = "kode_univ"),
            @Result(property = "code", column = "kode_prodi"),
            @Result(property = "name", column = "nama_prodi")})
    List<StudyProgram> selectStudyPrograms(@Param("univCode") String univCode);

    @Insert("INSERT INTO prodi(kode_univ, kode_prodi, nama_prodi) " +
            "VALUES (#{univCode}, #{code}, #{name})")
    void insertStudyProgram(StudyProgram studyProgram);

    @Delete("DELETE FROM prodi WHERE kode_prodi = #{code}")
    void deleteStudyProgram(@Param("code") String code);

    @Update("UPDATE prodi SET nama_prodi = #{name} WHERE kode_prodi = #{code}")
    void updateStudyProgram(StudyProgram studyProgram);
}
