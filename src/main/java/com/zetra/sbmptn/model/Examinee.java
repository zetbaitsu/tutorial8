package com.zetra.sbmptn.model;

import com.zetra.sbmptn.util.AgeCalculator;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created on : April 07, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Examinee {
    private String number;
    private String name;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthDate;
    private String programCode;

    public int getAge() {
        return AgeCalculator.calculateAge(birthDate, new Date());
    }
}
