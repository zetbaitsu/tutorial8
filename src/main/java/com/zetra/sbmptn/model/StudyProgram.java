package com.zetra.sbmptn.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created on : April 07, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudyProgram {
    private String univCode;
    private String code;
    private String name;
}
