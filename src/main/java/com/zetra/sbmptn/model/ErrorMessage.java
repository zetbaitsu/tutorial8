package com.zetra.sbmptn.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created on : April 08, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorMessage {
    private String title;
    private String description;
}
