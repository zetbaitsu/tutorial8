package com.zetra.sbmptn.controller;

import com.zetra.sbmptn.model.StudyProgram;
import com.zetra.sbmptn.service.StudyProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created on : April 22, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
@RestController
public class StudyProgramRestController {
    private final StudyProgramService studyProgramService;

    @Autowired
    public StudyProgramRestController(StudyProgramService studyProgramService) {
        this.studyProgramService = studyProgramService;
    }

    @RequestMapping(value = "/rest/prodi", method = RequestMethod.GET)
    public StudyProgram getStudyProgram(@RequestParam("kode") String code) {
        return studyProgramService.selectStudyProgram(code);
    }
}
