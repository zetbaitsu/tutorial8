package com.zetra.sbmptn.controller;

import com.zetra.sbmptn.model.ErrorMessage;
import com.zetra.sbmptn.service.ErrorServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created on : April 07, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
@Controller
public class CustomErrorController extends BaseController implements ErrorController {
    private final ErrorServiceImpl errorService;

    @Autowired
    public CustomErrorController(ErrorServiceImpl errorService) {
        this.errorService = errorService;
    }

    @RequestMapping("/error")
    public String renderErrorPage(Model model, HttpServletRequest request) {
        ErrorMessage errorMessage = errorService.generateErrorMessage(request);
        return error(model, errorMessage.getTitle(), errorMessage.getDescription());
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}