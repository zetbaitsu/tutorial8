package com.zetra.sbmptn.controller;

import com.zetra.sbmptn.model.Examinee;
import com.zetra.sbmptn.service.ExamineeService;
import com.zetra.sbmptn.service.StudyProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created on : April 07, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
@Controller
public class ExamineeController extends BaseController {
    private final ExamineeService examineeService;
    private final StudyProgramService studyProgramService;

    @Autowired
    public ExamineeController(ExamineeService examineeService, StudyProgramService studyProgramService) {
        this.examineeService = examineeService;
        this.studyProgramService = studyProgramService;
    }

    @RequestMapping("/peserta")
    public String viewDetail(Model model, @RequestParam("nomor") String number) {
        Examinee examinee = examineeService.selectExaminee(number);
        if (examinee != null) {
            model.addAttribute("examinee", examinee);
            return "examinee/detail";
        }
        return error(model, "Error", "Nomor peserta " + number + " tidak dapat ditemukan.");
    }

    @RequestMapping("/peserta/add")
    public String create() {
        return "examinee/create";
    }

    @RequestMapping("/peserta/add/{programCode}")
    public String create(Model model, @PathVariable("programCode") String programCode) {
        if (studyProgramService.selectStudyProgram(programCode) == null) {
            return error(model, "Error", "Program studi dengan kode " + programCode + " tidak dapat ditemukan.");
        }

        model.addAttribute("kode_prodi", programCode);
        return "examinee/create";
    }

    @RequestMapping(value = "/peserta/add/submit", method = RequestMethod.POST)
    public String createSubmit(Model model,
                               @RequestParam("nomor") String number,
                               @RequestParam("nama") String name,
                               @RequestParam("tgl_lahir") String birthDateStr,
                               @RequestParam("kode_prodi") String programCode) {

        if (examineeService.selectExaminee(number) != null) {
            return error(model, "Error", "Peserta dengan nomor " + number + " sudah ada!");
        }

        if (programCode == null || programCode.trim().isEmpty()) {
            programCode = null;
        } else if (studyProgramService.selectStudyProgram(programCode) == null) {
            return error(model, "Error", "Program studi dengan kode " + programCode + " tidak dapat ditemukan.");
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date birthDate;
        try {
            birthDate = dateFormat.parse(birthDateStr);
        } catch (ParseException e) {
            return error(model, "Error", "Format tanggal salah, silahkan gunakan format yyyy-MM-dd");
        }

        examineeService.insertExaminee(new Examinee(number, name, birthDate, programCode));
        return "redirect:/peserta?nomor=" + number;
    }

    @RequestMapping("/peserta/edit/{number}")
    public String edit(Model model, @PathVariable("number") String number) {
        Examinee examinee = examineeService.selectExaminee(number);
        if (examinee == null) {
            return error(model, "Error", "Peserta dengan nomor " + number + " tidak dapat ditemukan.");
        }

        model.addAttribute("examinee", examinee);
        return "examinee/edit";
    }

    @RequestMapping(value = "/peserta/edit/submit", method = RequestMethod.POST)
    public String editSubmit(Model model, Examinee examinee) {
        if (examinee.getProgramCode() == null || examinee.getProgramCode().trim().isEmpty()) {
            examinee.setProgramCode(null);
        } else if (studyProgramService.selectStudyProgram(examinee.getProgramCode()) == null) {
            return error(model, "Error", "Program studi dengan kode " + examinee.getProgramCode() + " tidak dapat ditemukan.");
        }
        examineeService.updateExaminee(examinee);
        return "redirect:/peserta?nomor=" + examinee.getNumber();
    }

    @RequestMapping("/peserta/delete/{number}")
    public String delete(Model model, @PathVariable("number") String number) {
        Examinee examinee = examineeService.selectExaminee(number);
        if (examinee == null) {
            return error(model, "Error", "Peserta dengan nomor " + number + " tidak dapat ditemukan.");
        }

        String programCode = examinee.getProgramCode();
        examineeService.deleteExaminee(number);
        return "redirect:/prodi?kode=" + programCode;
    }
}
