package com.zetra.sbmptn.controller;

import com.zetra.sbmptn.model.Examinee;
import com.zetra.sbmptn.model.StudyProgram;
import com.zetra.sbmptn.service.ExamineeService;
import com.zetra.sbmptn.service.StudyProgramService;
import com.zetra.sbmptn.service.UniversityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created on : April 07, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
@Controller
public class HomeController extends BaseController {
    private final ExamineeService examineeService;
    private final StudyProgramService studyProgramService;
    private final UniversityService universityService;

    @Autowired
    public HomeController(ExamineeService examineeService,
                          StudyProgramService studyProgramService,
                          UniversityService universityService) {
        this.examineeService = examineeService;
        this.studyProgramService = studyProgramService;
        this.universityService = universityService;
    }

    @RequestMapping("/")
    public String home() {
        return "home/home";
    }

    @RequestMapping(value = "/pengumumman/submit", method = RequestMethod.POST)
    public String result(Model model, @RequestParam("nomor") String number) {
        if (number == null || number.isEmpty()) {
            return error(model, "Pengumumman SBMPTN", "Mohon isi nomor peserta!");
        }

        Examinee examinee = examineeService.selectExaminee(number);
        if (examinee != null) {
            model.addAttribute("examinee", examinee);
            if (examinee.getProgramCode() != null) {
                StudyProgram studyProgram = studyProgramService.selectStudyProgram(examinee.getProgramCode());
                model.addAttribute("studyProgram", studyProgram);
                model.addAttribute("university", universityService.selectUniversity(studyProgram.getUnivCode()));
            }
            return "home/result";
        }

        return error(model, "Pengumumman SBMPTN", "Nomor peserta " + number + " tidak dapat ditemukan.");
    }
}
