package com.zetra.sbmptn.controller;

import com.zetra.sbmptn.model.Examinee;
import com.zetra.sbmptn.service.ExamineeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created on : April 22, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
@RestController
public class ExamineeRestController {
    private final ExamineeService examineeService;

    @Autowired
    public ExamineeRestController(ExamineeService examineeService) {
        this.examineeService = examineeService;
    }

    @RequestMapping(value = "/rest/peserta", method = RequestMethod.GET)
    public Examinee getExaminee(@RequestParam("nomor") String number) {
        return examineeService.selectExaminee(number);
    }
}
