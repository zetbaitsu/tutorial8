package com.zetra.sbmptn.controller;

import com.zetra.sbmptn.model.Examinee;
import com.zetra.sbmptn.model.StudyProgram;
import com.zetra.sbmptn.model.University;
import com.zetra.sbmptn.service.ExamineeService;
import com.zetra.sbmptn.service.StudyProgramService;
import com.zetra.sbmptn.service.UniversityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created on : April 07, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
@Controller
public class StudyProgramController extends BaseController {
    private final StudyProgramService studyProgramService;
    private final UniversityService universityService;
    private final ExamineeService examineeService;

    @Autowired
    public StudyProgramController(StudyProgramService studyProgramService,
                                  UniversityService universityService,
                                  ExamineeService examineeService) {
        this.studyProgramService = studyProgramService;
        this.universityService = universityService;
        this.examineeService = examineeService;
    }

    @RequestMapping("/prodi")
    public String viewDetail(Model model, @RequestParam("kode") String code) {
        StudyProgram studyProgram = studyProgramService.selectStudyProgram(code);
        if (studyProgram != null) {
            model.addAttribute("program", studyProgram);
            model.addAttribute("university", universityService.selectUniversity(studyProgram.getUnivCode()));
            List<Examinee> examinees = examineeService.selectProgramExaminees(code);
            if (examinees != null && !examinees.isEmpty()) {
                model.addAttribute("examinees", examinees);
                model.addAttribute("youngest", examinees
                        .parallelStream()
                        .max((o1, o2) -> o1.getBirthDate().compareTo(o2.getBirthDate()))
                        .get());
                model.addAttribute("oldest", examinees
                        .parallelStream()
                        .min((o1, o2) -> o1.getBirthDate().compareTo(o2.getBirthDate()))
                        .get());
            }
            return "studyprogram/detail";
        }
        return error(model, "Error", "Prodi dengan kode " + code + " tidak dapat ditemukan.");
    }

    @RequestMapping("/prodi/add/{univCode}")
    public String create(Model model, @PathVariable("univCode") String univCode) {
        University university = universityService.selectUniversity(univCode);
        if (university == null) {
            return error(model, "Error", "Universitas dengan kode " + univCode + " tidak dapat ditemukan.");
        }

        model.addAttribute("kode_univ", univCode);
        return "studyprogram/create";
    }

    @RequestMapping(value = "/prodi/add/submit", method = RequestMethod.POST)
    public String createSubmit(Model model,
                               @RequestParam("kode_univ") String univCode,
                               @RequestParam("kode") String code,
                               @RequestParam("nama") String name) {

        if (studyProgramService.selectStudyProgram(code) != null) {
            return error(model, "Error", "Prodi dengan kode " + code + " sudah ada!");
        }

        studyProgramService.insertStudyProgram(new StudyProgram(univCode, code, name));
        return "redirect:/univ/" + univCode;
    }

    @RequestMapping("/prodi/edit/{code}")
    public String edit(Model model, @PathVariable("code") String code) {
        StudyProgram studyProgram = studyProgramService.selectStudyProgram(code);
        if (studyProgram == null) {
            return error(model, "Error", "Prodi dengan kode " + code + " tidak dapat ditemukan.");
        }

        model.addAttribute("studyProgram", studyProgram);
        return "studyprogram/edit";
    }

    @RequestMapping(value = "/prodi/edit/submit", method = RequestMethod.POST)
    public String editSubmit(StudyProgram studyProgram) {
        studyProgramService.updateStudyProgram(studyProgram);
        return "redirect:/prodi?kode=" + studyProgram.getCode();
    }

    @RequestMapping("/prodi/delete/{code}")
    public String delete(Model model, @PathVariable("code") String code) {
        StudyProgram studyProgram = studyProgramService.selectStudyProgram(code);
        if (studyProgram == null) {
            return error(model, "Error", "Prodi dengan kode " + code + " tidak dapat ditemukan.");
        }

        String univCode = studyProgram.getUnivCode();
        studyProgramService.deleteStudyProgram(code);
        return "redirect:/univ/" + univCode;
    }
}
