package com.zetra.sbmptn.controller;

import com.zetra.sbmptn.model.Examinee;
import com.zetra.sbmptn.model.University;
import com.zetra.sbmptn.service.ExamineeService;
import com.zetra.sbmptn.service.StudyProgramService;
import com.zetra.sbmptn.service.UniversityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created on : April 07, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
@Controller
public class UniversityController extends BaseController {
    private final UniversityService universityService;
    private final StudyProgramService studyProgramService;
    private final ExamineeService examineeService;

    @Autowired
    public UniversityController(UniversityService universityService,
                                StudyProgramService studyProgramService,
                                ExamineeService examineeService) {
        this.universityService = universityService;
        this.studyProgramService = studyProgramService;
        this.examineeService = examineeService;
    }

    @RequestMapping("/univ")
    public String viewAll(Model model) {
        model.addAttribute("universities", universityService.selectAllUniversities());
        return "university/viewAll";
    }

    @RequestMapping("/univ/{code}")
    public String viewDetail(Model model, @PathVariable("code") String code) {
        University university = universityService.selectUniversity(code);
        if (university != null) {
            model.addAttribute("university", university);
            model.addAttribute("studyPrograms", studyProgramService.selectStudyPrograms(code));
            return "university/detail";
        }
        return error(model, "Error", "Universitas dengan kode " + code + " tidak dapat ditemukan.");
    }

    @RequestMapping("/univ/{code}/peserta")
    public String viewExaminees(Model model, @PathVariable("code") String code) {
        University university = universityService.selectUniversity(code);
        if (university != null) {
            model.addAttribute("university", university);
            List<Examinee> examinees = examineeService.selectUniversityExaminees(code);
            if (examinees != null && !examinees.isEmpty()) {
                model.addAttribute("examinees", examinees);
                model.addAttribute("youngest", examinees
                        .parallelStream()
                        .max((o1, o2) -> o1.getBirthDate().compareTo(o2.getBirthDate()))
                        .get());
                model.addAttribute("oldest", examinees
                        .parallelStream()
                        .min((o1, o2) -> o1.getBirthDate().compareTo(o2.getBirthDate()))
                        .get());
            }
            return "university/examinees";
        }
        return error(model, "Error", "Universitas dengan kode " + code + " tidak dapat ditemukan.");
    }

    @RequestMapping("/univ/add")
    public String create() {
        return "university/create";
    }

    @RequestMapping(value = "/univ/add/submit", method = RequestMethod.POST)
    public String createSubmit(Model model,
                               @RequestParam("kode") String code,
                               @RequestParam("nama") String name,
                               @RequestParam("url") String url) {

        if (universityService.selectUniversity(code) != null) {
            return error(model, "Error", "Universitas dengan kode " + code + " sudah ada!");
        }

        universityService.insertUniversity(new University(code, name, url));
        return "redirect:/univ/" + code;
    }

    @RequestMapping("/univ/edit/{code}")
    public String edit(Model model, @PathVariable("code") String code) {
        University university = universityService.selectUniversity(code);
        if (university == null) {
            return error(model, "Error", "Universitas dengan kode " + code + " tidak dapat ditemukan.");
        }

        model.addAttribute("university", university);
        return "university/edit";
    }

    @RequestMapping(value = "/univ/edit/submit", method = RequestMethod.POST)
    public String editSubmit(University university) {
        universityService.updateUniversity(university);
        return "redirect:/univ/" + university.getCode();
    }

    @RequestMapping("/univ/delete/{code}")
    public String delete(Model model, @PathVariable("code") String code) {
        if (universityService.selectUniversity(code) == null) {
            return error(model, "Error", "Universitas dengan kode " + code + " tidak dapat ditemukan.");
        }

        universityService.deleteUniversity(code);
        return "redirect:/univ";
    }
}
