package com.zetra.sbmptn.controller;

import org.springframework.ui.Model;

/**
 * Created on : April 08, 2017
 * Author     : zetbaitsu
 * Name       : Zetra
 * NPM        : 1606955076
 * GitHub     : https://github.com/zetbaitsu
 */
abstract class BaseController {
    protected String error(Model model, String title, String description) {
        model.addAttribute("title", title);
        model.addAttribute("description", description);
        return "errors";
    }
}
